#include "Person.h"


Person::Person(std::string name, uint32_t id) : m_name(name), m_id(id)
{}

uint32_t Person::get_id() const
{
    return m_id;
}

std::string Person::get_name() const
{
    return m_name;
}
