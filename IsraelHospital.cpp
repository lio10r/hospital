#include "IsraelHospital.h"

IsraelHospital::IsraelHospital(std::string name, uint32_t bed_number, uint8_t max_percent) : Hospital(name, bed_number), m_max_percent(max_percent)
{}


bool IsraelHospital::add_patient(const Patient patient)
{
    // makes a calculation of the real max capacity, (m_max_percent / 100)
    add_person<Patient>((&m_patients), patient, ((m_bed_number * m_max_percent) / HUDNRED));
}