#ifndef _H_PERSON
#define _H_PERSON

#include "Definitions.h"

class Person
{
public:
    Person(std::string name, uint32_t id);

    uint32_t get_id() const;

    std::string get_name() const;

protected:
    std::string m_name;
    uint32_t m_id;
};

#endif // _H_PERSON