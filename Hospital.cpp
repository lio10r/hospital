#include "Hospital.h"

Hospital::Hospital(std::string name, size_t beds_number) : m_name(name), m_bed_number(beds_number)
{}


bool Hospital::add_patient(const Patient patient)
{
    return add_person<Patient>((&m_patients), patient, m_bed_number);
}


bool Hospital::add_employee(const Employee employee)
{
    return add_person<Employee>((&m_employees), employee, MAX_EMPLOYEE_NUMBER);
}

template <class T>
bool Hospital::add_person(std::vector<T>* v_add_person, const T person, size_t max_capacity)
{
    // check if vector is at max capacity
    if (v_add_person->size() == max_capacity)
    {
        printf(REACHED_MAX_CAPACITY, m_name.c_str());
        return false;
    }

    // check if person is already in the system
    for (auto it = v_add_person->begin(); it != v_add_person->end(); it++)
    {
        if (it->get_id() == person.get_id())
        {
            printf(EXISTING_PERSON, person.get_id());
            return false;
        }
    }

    v_add_person->push_back(person);
    return true;
}
