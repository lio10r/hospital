#ifndef _H_PATIENT
#define _H_PATIENT

#include "Person.h"

class Patient : public Person
{
public:
    Patient(std::string name, uint32_t id) : Person(name, id) {}
};

#endif //_H_PATIENT