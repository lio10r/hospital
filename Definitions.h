#ifndef _H_DEFENITIONS
#define _H_DEFENITIONS

#include <vector>
#include <string>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

constexpr char REACHED_MAX_CAPACITY[] = "Hospital %s is at 100%% capacity of employees or patients\n";
constexpr char EXISTING_PERSON[] = "%d is already in the system\n";

#endif // _H_DEFENISIONS