#include "Definitions.h"
#include "Employee.h"
#include "Patient.h"

constexpr size_t MAX_EMPLOYEE_NUMBER = 250;

class Hospital
{
public:
    Hospital(std::string name, size_t beds_number);


    /**
     * a function to add a patient to the system
     * @param patient       the patient to add
     * @return              true on success, false on failure
     * 
     * @note  trying to add the same patient twice will cause a failure, and reaching max capacity
     */
    virtual bool add_patient(const Patient patient);

    /**
     * a function to add an employee to the system
     * @param employee       the employee to add
     * @return               true on success, false on failure
     * 
     * @note  trying to add the same employee twice will cause a failure, and reaching max capacity
     */
    bool add_employee(const Employee employee);


protected:

    /**
     * a helper function to add a person to one of the vectors in the data members
     * 
     * @param v_add_person      the vector to add to (m_patients or m_employees)
     * @param person            the person to add
     * @param max_capacity      the max capacity of the vector
     * 
     * @return                  true on success, false on failure
     */
    template <class T>
    bool add_person(std::vector<T>* v_add_person, const T person, size_t max_capacity);

    std::string m_name;
    size_t m_bed_number;
    std::vector<Patient> m_patients;
    std::vector<Employee> m_employees;
};