#include "Person.h"

class Employee : public Person
{
public:
    Employee(std::string name, uint32_t id) : Person(name, id) {}
};