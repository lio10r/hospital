#include "Hospital.h"

// this const is for calculation of percents of 100
constexpr uint8_t HUDNRED = 100;

class IsraelHospital : public Hospital
{
public:
    IsraelHospital(std::string name, uint32_t bed_number, uint8_t max_percent);

    /**
     * @see Hospital
     * @note the calculation of max capacity take m_max_percent into account
     */
    virtual bool add_patient(const Patient patient) override;

private:
    // this is the percent that the hospital can hold (if 120 it can hold 20 percent more patients)
    uint8_t m_max_percent;
};